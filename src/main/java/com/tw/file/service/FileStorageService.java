package com.tw.file.service;


import com.tw.file.entity.FileData;
import com.tw.file.repository.FileStorageRepository;
import com.tw.file.util.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
public class FileStorageService {

    @Autowired
    private FileStorageRepository repository;

    public String uploadFile(MultipartFile file) throws IOException {

        if (repository.findByName(file.getOriginalFilename()).isPresent())
            if (file.getOriginalFilename().equals(repository.findByName(file.getOriginalFilename()).get().getName())) {
                throw new RuntimeException("File  with similar name already exists");
            }


        FileData fileData = repository.save(FileData.builder()
                .name(file.getOriginalFilename())
                .type(file.getContentType())
                .fileData(FileUtils.compressFile(file.getBytes())).build());
        if (fileData != null) {
            return "file uploaded successfully :- " + file.getOriginalFilename();
        }
        throw new RuntimeException("file not uploaded successfully");
    }

    public FileData downloadFile(String fileName) {
        FileData dbFileData = repository.findByName(fileName).get();
        byte[] files = FileUtils.decompressFileData(dbFileData.getFileData());
        return FileData.builder()
                .name(dbFileData.getName())
                .type(dbFileData.getType())
                .fileData(files)
                .build();
    }

    public void deleteFile(String fileName) {
        Optional<FileData> dbFileData = repository.findByName(fileName);
        repository.delete(dbFileData.get());
    }
}
