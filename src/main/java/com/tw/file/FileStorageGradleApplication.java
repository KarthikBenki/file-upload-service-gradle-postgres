package com.tw.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileStorageGradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileStorageGradleApplication.class, args);
	}

}
